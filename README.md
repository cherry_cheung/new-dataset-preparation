# new-dataset-preparation
![new_dataset_preparation.drawio.svg](docs/new_dataset_preparation.drawio.svg)


## How to run the repo?
### config instruction
```
mongo_db_params: {
   'url': '',
   'collection': 'ml-hub-cctv'
}

mongo_collection_name: 'apps'

event_center_db_params: {
    'database': 'event_center',
    'port': 5432,
    'username': '',
    'host': 'core-db-prod1.cyrxl1qw3cja.ap-southeast-1.rds.amazonaws.com',
    'password': ''
}

event_center_schema_name: 'event_center'

mlops_db_params: {
   'database': 'public',
   'port': 5432,
   'username': '',
   'host': 'core-db-prod1.cyrxl1qw3cja.ap-southeast-1.rds.amazonaws.com',
   'password': ''
}

mlops_schema_name: 'public'

aws_credential: {
                  aws_access_key_id: '',
                  aws_secret_access_key: '',
                  region_name: 'ap-southeast-1',
                  bucket_name: 'ai-cam-detection-result'
}

# target path of the images folder
split_local_folder_dir: ''

upload_aws_credential: {
                  aws_access_key_id: "",
                  aws_secret_access_key: "",
                  region_name: 'ap-southeast-1',
                  bucket_name: 'varadise-mlops-image-uploader-test',
                  s3_folder: ''
}

# model path of the previous yolo model
model: {
              'P001_helmet': '<model_path>',
              ...
}

# optional
annotated_flag: True

# temporary only support YoloLabel format
# target path of the annotated images folder
annotated_images_folder_path: ''
```

```
touch config.yaml
# Adhere to the instructions and insert the relevant information into the configuration.
python3 main.py
```

## Expected Result
Within the split_local_folder_dir, the folder structure is as follows:
```
- split_folder_dir
  - Project_id_1
    - Images_1
    - Images_2
    - ...
  - Project_id_2
    - Images_1
    - Images_2
    - ...
   - ...
```
(Optional) Within the  annotated_images_folder_path,  the folder structure is as follows:
```
- split_folder_dir
  - Project_id_1
    - Images_1
    - Labels_1
    - Images_2
    - Labels_2
    - ...
  - Project_id_2
    - Images_1
    - Labels_1
    - Images_2
    - Labels_2
    - ...
   - ...
```
- The fp_staging table is updated.
- The images folders are pushed to S3. 