import uuid

from sqlalchemy import Column, DateTime, JSON, text, Text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql.expression import cast

from app.utils.db_connection import Base


class Events(Base):
    __tablename__ = 'events'
    __table_args__ = {'schema': 'event_center'}

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
    message = Column(JSON)
    event_time = Column(DateTime(timezone=True))

    @hybrid_property
    def sensor_type(self):
        return self.message.get('sensorType')

    @sensor_type.expression
    def sensor_type(cls):
        return cast(text(f"message->>'sensorType'"), Text)

    @hybrid_property
    def first_photo_url(self):
        return self.message.get('photoURLs', [])[0] if self.message.get('photoURLs') else None

    @first_photo_url.expression
    def first_photo_url(cls):
        return cast(text(f"message->'photoURLs'->>0"), Text)


class FalseAlarms(Base):
    __tablename__ = 'false_alarms'
    __table_args__ = {'schema': 'event_center'}

    event_id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
