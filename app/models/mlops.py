import uuid

from sqlalchemy import Column, DateTime, Text
from sqlalchemy.dialects.postgresql import UUID

from app.utils.db_connection import Base


class FPStaging(Base):
    __tablename__ = "fp_staging"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
    photo_url = Column(Text)
    labour_action_date = Column(DateTime(timezone=True))
    dispatched_at = Column(DateTime(timezone=True))
    source_id = Column(UUID(as_uuid=True), default=uuid.uuid4)


class Dataset(Base):
    __tablename__ = "dataset"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
