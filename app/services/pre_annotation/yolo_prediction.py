import os
import glob
import shutil

from ultralytics import YOLO
from tqdm import tqdm


class PreAnnotation:
    def __init__(self, config):
        self.model_dict = config['model']
        self.images_folder_path = config['split_local_folder_dir']
        self.annotated_images_folder_path = config['annotated_images_folder_path']

    def make_a_copy_of_folder(self):
        if os.path.exists(self.annotated_images_folder_path):
            shutil.rmtree(self.annotated_images_folder_path)
        shutil.copytree(self.images_folder_path, self.annotated_images_folder_path)

    def predict_one_project(self, model_path, one_images_folder_dir):
        temporary_folder_name = 'temp'
        temporary_folder_path = os.path.join(os.getcwd(), temporary_folder_name)
        if os.path.exists(temporary_folder_path):
            shutil.rmtree(temporary_folder_path)

        model = YOLO(model_path)
        for image in tqdm(os.listdir(one_images_folder_dir)):
            if image.endswith('.jpg'):
                image_path = os.path.join(one_images_folder_dir, image)
                model.predict(image_path, save_txt=True, conf=0.3, iou=0.7, project=temporary_folder_name)

        txt_files = glob.glob(temporary_folder_path + '/**/*.txt', recursive=True)

        for txt_file in txt_files:
            shutil.move(txt_file, one_images_folder_dir)

        shutil.rmtree(temporary_folder_path)

    def predict_all_projects(self):
        self.make_a_copy_of_folder()
        for sub_folder in os.listdir(self.annotated_images_folder_path):
            sub_folder_path = os.path.join(self.annotated_images_folder_path, sub_folder)
            if sub_folder in self.model_dict.keys():
                model_path = self.model_dict[sub_folder]
                self.predict_one_project(model_path, sub_folder_path)
