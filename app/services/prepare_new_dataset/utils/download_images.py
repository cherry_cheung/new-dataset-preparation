import os
import shutil
import boto3
from tqdm import tqdm

from app.utils.query_mongo_apps import MongoModelID
from app.utils.log_error import *


class ImageDownloader:
    def __init__(self, aws_param, s3_download_local_folder_dir, split_local_folder_dir, mongo_db_params,
                 mongo_collection_name):
        self.aws_param = aws_param
        self.aws_access_key_id = aws_param['aws_access_key_id']
        self.aws_secret_access_key = aws_param['aws_secret_access_key']
        self.region_name = aws_param['region_name']
        self.bucket_name = aws_param['bucket_name']
        self.s3_download_local_folder_dir = s3_download_local_folder_dir
        self.split_local_folder_dir = split_local_folder_dir
        self.mongo_db_params = mongo_db_params
        self.mongo_collection_name = mongo_collection_name

    def clear_local_folder_dir(self, folder_path):
        if os.path.exists(folder_path):
            shutil.rmtree(folder_path)
        os.makedirs(folder_path)

    def download_one_image_from_s3(self, url, id_):
        object_key = url.split('https://ai-cam-detection-result.s3.ap-southeast-1.amazonaws.com/')[1]

        s3 = boto3.client('s3',
                          aws_access_key_id=self.aws_access_key_id,
                          aws_secret_access_key=self.aws_secret_access_key,
                          region_name=self.region_name)

        local_file_path = os.path.join(self.s3_download_local_folder_dir, str(id_) + '_' + url.split('/')[-1])
        s3.download_file(self.bucket_name, object_key, local_file_path)

    def download_images_from_s3(self, results):
        self.clear_local_folder_dir(self.s3_download_local_folder_dir)
        for result in tqdm(results):
            try:
                id_ = result[1]
                url = result[0].replace('original', 'labeled')
                self.download_one_image_from_s3(url, id_)
            except:
                logging.warning(f'Fail to download alert from S3: Variable: {result}')

    def split_2_project(self):
        if os.path.exists(self.split_local_folder_dir):
            shutil.rmtree(self.split_local_folder_dir)
        os.makedirs(self.split_local_folder_dir)

        for image in os.listdir(self.s3_download_local_folder_dir):
            try:
                original_image_path = os.path.join(self.s3_download_local_folder_dir, image)
                monitor_id = image.split('_')[3]
                mongo_model_id = MongoModelID(self.mongo_db_params, self.mongo_collection_name)
                model_name = mongo_model_id.get_model_name(monitor_id)
                sub_folder_dir = os.path.join(self.split_local_folder_dir, model_name)
                if not os.path.exists(sub_folder_dir):
                    os.makedirs(sub_folder_dir)
                image_path = os.path.join(sub_folder_dir, image)
                shutil.move(original_image_path, image_path)
            except:
                logging.warning(f'Fail to split to project dir: Variable : {image}')

        shutil.rmtree(self.s3_download_local_folder_dir)
