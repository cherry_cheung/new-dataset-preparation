import os
from PIL import Image


class RenameFileExtension:
    def __init__(self, image_folder_path):
        self.image_folder_path = image_folder_path

    def change_image_file_extension(self):
        for sub_folder in os.listdir(self.image_folder_path):
            sub_folder_path = os.path.join(self.image_folder_path, sub_folder)
            for image in os.listdir(sub_folder_path):
                image_path = os.path.join(self.image_folder_path, sub_folder, image)
                if image.endswith('.jpg'):
                    pass
                else:
                    img = Image.open(image_path)
                    output_filename = os.path.splitext(image)[0] + ".jpg"
                    output_path = os.path.join(self.image_folder_path, output_filename)
                    img.save(output_path, "JPEG")
                    os.remove(image_path)

    def rename_image_folders(self):
        for sub_folder in os.listdir(self.image_folder_path):
            project_id = sub_folder.split('_')[0]
            sub_folder_path = os.path.join(self.image_folder_path, sub_folder)
            for image in os.listdir(sub_folder_path):
                image_path = os.path.join(sub_folder_path, image)
                source_id = image.split('_')[0]
                date = image.split('_')[1]
                time = image.split('_')[2]
                cam_id = image.split('_')[3]
                new_image_name = date + '_' + time + '_' + project_id + '_' + cam_id + '_' + source_id + '.jpg'
                new_image_path = os.path.join(sub_folder_path, new_image_name)
                os.rename(image_path, new_image_path)

    def rename_execute(self):
        self.change_image_file_extension()
        self.rename_image_folders()
