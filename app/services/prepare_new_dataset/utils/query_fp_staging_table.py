from sqlalchemy import select

from app.models.mlops import FPStaging
from app.utils.db_connection import high_level_postgres_connect


class QueryFPStagingTable:
    def __init__(self, db_params):
        self.db_params = db_params

    def query_not_yet_handled_alert(self):
        with high_level_postgres_connect(self.db_params) as session:
            query = select(FPStaging.photo_url, FPStaging.source_id).where(FPStaging.dispatched_at.is_(None))
            results = session.execute(query).fetchall()

            return results
