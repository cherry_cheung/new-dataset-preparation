import os

from datetime import datetime, timezone
from sqlalchemy import update

from app.models.mlops import FPStaging
from app.utils.db_connection import high_level_postgres_connect
from app.utils.log_error import *


class UpdateFPStagingTable:
    def __init__(self, folder_dir, db_params):
        self.folder_dir = folder_dir
        self.db_params = db_params

    def update_fp_db(self):
        for sub_folder in os.listdir(self.folder_dir):
            sub_folder_path = os.path.join(self.folder_dir, sub_folder)
            for image in os.listdir(sub_folder_path):
                try:
                    image_path = os.path.join(sub_folder_path, image)
                    sample_id = image_path.split('/')[-1].split('_')[0]
                    with high_level_postgres_connect(self.db_params) as session:
                        update_stmt = update(FPStaging).where(FPStaging.source_id == sample_id).values(
                            dispatched_at=datetime.now(timezone.utc))
                        session.execute(update_stmt)
                        session.commit()
                except:
                    logging.warning(f'Fail to update alert in fp_staging: Variable: {image}')

