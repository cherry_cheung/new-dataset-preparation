import os

from app.services.prepare_new_dataset.utils.query_fp_staging_table import QueryFPStagingTable
from app.services.prepare_new_dataset.utils.update_fp_staging_table import UpdateFPStagingTable
from app.services.prepare_new_dataset.utils.download_images import ImageDownloader
from app.services.prepare_new_dataset.utils.rename_file_extension import RenameFileExtension


class PrepareNewImagesFolder:
    def __init__(self, config):
        self.aws_credential = config['aws_credential']
        self.split_local_folder_dir = config['split_local_folder_dir']
        self.s3_download_local_folder_dir = os.path.join(os.path.dirname(self.split_local_folder_dir), 'new_data')
        self.mongo_db_params = config['mongo_db_params']
        self.mongo_collection_name = config['mongo_collection_name']
        self.mlops_db_params = config['mlops_db_params']

    def prepare_new_dataset_folder(self):
        print('Start query not yet handled alert')
        query_fpstaging_table = QueryFPStagingTable(self.mlops_db_params)
        result = query_fpstaging_table.query_not_yet_handled_alert()

        image_downloader = ImageDownloader(self.aws_credential, self.s3_download_local_folder_dir,
                                           self.split_local_folder_dir,
                                           self.mongo_db_params,
                                           self.mongo_collection_name)
        print('Start download images from s3')
        image_downloader.download_images_from_s3(result)
        print('Split images into projects')
        image_downloader.split_2_project()

        print('Update fp_db after prepare new images dataset')
        update_fp_staging_table = UpdateFPStagingTable(self.split_local_folder_dir, self.mlops_db_params)
        update_fp_staging_table.update_fp_db()

        print('Start rename and change image file extension')
        rename_file_extension = RenameFileExtension(self.split_local_folder_dir)
        rename_file_extension.rename_execute()
