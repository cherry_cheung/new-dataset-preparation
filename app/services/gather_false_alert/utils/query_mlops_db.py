from app.utils.db_connection import high_level_postgres_connect
from app.models.mlops import FPStaging


class QueryFPStaging:
    def __init__(self, db_params):
        self.db_params = db_params

    def query_fp_staging_table(self):
        with high_level_postgres_connect(self.db_params) as session:
            fp_staging_source_id = session.query(FPStaging.source_id)
            fp_staging_source_id_result = session.execute(fp_staging_source_id).fetchall()

            return fp_staging_source_id_result
