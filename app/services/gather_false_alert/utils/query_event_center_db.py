from sqlalchemy import select

from app.utils.db_connection import low_level_postgres_connect
from app.models.event_center import Events, FalseAlarms


class QueryEventCenter:
    def __init__(self, db_params):
        self.db_params = db_params

    def query_events_false_alarm_tables(self):
        with low_level_postgres_connect(self.db_params) as postgres_db:
            alarms_query = (
                select(Events.id, Events.message, Events.event_time)
                .join(FalseAlarms, Events.id == FalseAlarms.event_id)
                .filter(
                    (Events.sensor_type == "SOURCE_TYPE_AICAM_OBJ_DETECTION") &
                    (Events.first_photo_url !=
                     "https://ai-cam-detection-result.s3.ap-southeast-1.amazonaws.com/https://event-snapshot.s3.us-west-2.amazonaws.com/alert-icon/MsgImage_011_Alert_General.png")
                )
            )

            alert_result = postgres_db.execute(alarms_query).fetchall()

            return alert_result
