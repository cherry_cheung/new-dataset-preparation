import uuid
import pandas as pd
from datetime import datetime, timezone

from app.utils.db_connection import low_level_postgres_connect
from app.utils.query_mongo_apps import MongoModelID
from app.services.gather_false_alert.utils.query_mlops_db import QueryFPStaging
from app.services.gather_false_alert.utils.query_event_center_db import QueryEventCenter

from app.utils.log_error import *


class GatherFalseAlert:
    def __init__(self, config):
        self.mongo_params = config['mongo_db_params']
        self.mongo_collection_name = config['mongo_collection_name']
        self.mlops_params = config['mlops_db_params']
        self.mlops_schema_name = config['mlops_schema_name']
        self.event_center_params = config['event_center_db_params']
        self.fp_staging_id_result = QueryFPStaging(self.mlops_params).query_fp_staging_table()
        self.alert_result = QueryEventCenter(self.event_center_params).query_events_false_alarm_tables()

    def prepare_new_alert(self):
        alert_ids = set(row[0] for row in self.fp_staging_id_result)
        filtered_alert_result = [row for row in self.alert_result if row[0] not in alert_ids]

        id_list = []
        photo_url_list = []
        model_id_list = []
        cam_id_list = []
        reported_at_list = []
        imported_at_list = []
        created_at_list = []
        updated_at_list = []
        source_id_list = []

        for row in filtered_alert_result:
            try:
                id_list.append(str(uuid.uuid4()))
                photo_url = row[1]['photoURLs'][0]
                photo_url_list.append(photo_url)
                cam_id = photo_url.split('/')[3]
                cam_id_list.append(cam_id)
                mongo_model_id = MongoModelID(self.mongo_params, self.mongo_collection_name)
                model_name = mongo_model_id.get_model_name(cam_id)
                model_id_list.append(model_name)
                reported_at = row[2]
                reported_at_list.append(reported_at)
                imported_at_list.append(datetime.now(timezone.utc))
                created_at_list.append(datetime.now(timezone.utc))
                updated_at_list.append(datetime.now(timezone.utc))
                source_id = row[0]
                source_id_list.append(source_id)
            except:
                logging.warning(f'Fail to insert alert to fp_staging: Variable: {row}')

        df = pd.DataFrame({
            'id': id_list,
            'source_id': source_id_list,
            'photo_url': photo_url_list,
            'model_id': model_id_list,
            'cam_id': cam_id_list,
            'reported_at': reported_at_list,
            'imported_at': imported_at_list,
            'updated_at': updated_at_list,
            'created_at': created_at_list
        })

        return df

    def insert_new_false_alert(self):
        with low_level_postgres_connect(self.mlops_params) as postgres_db:
            df = self.prepare_new_alert()
            df.to_sql(name='fp_staging', con=postgres_db, schema=self.mlops_schema_name, if_exists='append',
                      index=False)
