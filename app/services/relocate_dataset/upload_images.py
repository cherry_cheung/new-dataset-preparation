import os
import boto3
from datetime import datetime


class S3Uploader:
    def __init__(self, config):
        self.aws_access_key_id = config['upload_aws_credential']['aws_access_key_id']
        self.aws_secret_access_key = config['upload_aws_credential']['aws_secret_access_key']
        self.region_name = config['upload_aws_credential']['region_name']
        self.bucket_name = config['upload_aws_credential']['bucket_name']
        self.s3_folder = config['upload_aws_credential']['s3_folder']
        self.split_local_folder_dir = config['split_local_folder_dir']
        self.annotated_images_folder_path = config['annotated_images_folder_path']
        self.annotated_flag = config['annotated_flag']

    def today_date(self):
        current_date = datetime.now().strftime("%Y%m%d")

        return current_date

    def s3_upload_one_folder(self, s3_folder, s3_sub_folder, local_folder_path):
        s3 = boto3.client('s3', aws_access_key_id=self.aws_access_key_id,
                          aws_secret_access_key=self.aws_secret_access_key,
                          region_name=self.region_name)
        for root, dirs, files in os.walk(local_folder_path):
            for file in files:
                local_file_path = os.path.join(root, file)
                s3_key = os.path.join(s3_folder, self.today_date(), s3_sub_folder,
                                      os.path.relpath(local_file_path, local_folder_path))
                s3.upload_file(local_file_path, self.bucket_name, s3_key)

    def s3_upload_folders(self):
        self.s3_upload_one_folder(self.s3_folder, 'images', self.split_local_folder_dir)
        if self.annotated_flag:
            self.s3_upload_one_folder(self.s3_folder, 'annotated', self.annotated_images_folder_path)
