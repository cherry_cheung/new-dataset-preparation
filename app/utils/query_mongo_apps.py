from app.utils.db_connection import mongodb_connect


class MongoModelID:
    def __init__(self, mongo_db_params, collection_name):
        self.mongo_db_params = mongo_db_params
        self.collection_name = collection_name

    def get_model_name(self, monitor_id):
        with mongodb_connect(self.mongo_db_params) as db:
            collection = db[self.collection_name]
            query = {'config.monitor_id': monitor_id}
            document = collection.find_one(query)
            model_id = document['config']['pgie']['model']

            return model_id
