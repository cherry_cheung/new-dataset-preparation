import yaml


def read_config(yaml_folder_path):
    with open(yaml_folder_path, 'r') as file:
        yaml_data = yaml.safe_load(file)
        config = {
            'aws_credential': {
                'aws_access_key_id': yaml_data['aws_credential']['aws_access_key_id'],
                'aws_secret_access_key': yaml_data['aws_credential']['aws_secret_access_key'],
                'region_name': yaml_data['aws_credential']['region_name'],
                'bucket_name': yaml_data['aws_credential']['bucket_name']
            },
            'upload_aws_credential': {
                'aws_access_key_id': yaml_data['upload_aws_credential']['aws_access_key_id'],
                'aws_secret_access_key': yaml_data['upload_aws_credential']['aws_secret_access_key'],
                'region_name': yaml_data['upload_aws_credential']['region_name'],
                'bucket_name': yaml_data['upload_aws_credential']['bucket_name'],
                's3_folder': yaml_data['upload_aws_credential']['s3_folder']
            },
            'mlops_db_params': {
                'database': yaml_data['mlops_db_params']['database'],
                'port': yaml_data['mlops_db_params']['port'],
                'username': yaml_data['mlops_db_params']['username'],
                'host': yaml_data['mlops_db_params']['host'],
                'password': yaml_data['mlops_db_params']['password']
            },
            'event_center_db_params': {
                'database': yaml_data['event_center_db_params']['database'],
                'port': yaml_data['event_center_db_params']['port'],
                'username': yaml_data['event_center_db_params']['username'],
                'host': yaml_data['event_center_db_params']['host'],
                'password': yaml_data['event_center_db_params']['password']
            },
            'mongo_db_params': {
                'url': yaml_data['mongo_db_params']['url'],
                'collection': yaml_data['mongo_db_params']['collection']
            },
            'mongo_collection_name': yaml_data['mongo_collection_name'],
            'mlops_schema_name': yaml_data['mlops_schema_name'],
            'event_center_schema_name': yaml_data['event_center_schema_name'],
            'split_local_folder_dir': yaml_data['split_local_folder_dir'],
            'model': yaml_data['model'],
            'annotated_flag': yaml_data['annotated_flag'],
            'annotated_images_folder_path': yaml_data['annotated_images_folder_path']
        }

        return config
