import os

from app.services.gather_false_alert.gather_new_alert import GatherFalseAlert
from app.services.prepare_new_dataset.prepare_new_images_folder import PrepareNewImagesFolder
from app.utils.config import read_config
from app.services.pre_annotation.yolo_prediction import PreAnnotation
from app.services.relocate_dataset.upload_images import S3Uploader


def main():
    yaml_folder_path = os.path.join(os.path.dirname(os.getcwd()), 'config.yaml')
    config = read_config(yaml_folder_path)

    gather_false_alert = GatherFalseAlert(config)
    gather_false_alert.insert_new_false_alert()

    prepare_new_images_folder = PrepareNewImagesFolder(config)
    prepare_new_images_folder.prepare_new_dataset_folder()

    if config['annotated_flag']:
        pre_annotate = PreAnnotation(config)
        pre_annotate.predict_all_projects()

    s3uploader = S3Uploader(config)
    s3uploader.s3_upload_folders()


if __name__ == "__main__":
    main()
